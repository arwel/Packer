from actions.action import Action
from pathlib import Path
from services.index import Index


class IndexAction(Action):
    def run(self):
        current_path = (Path('.')).cwd()
        config_path = str(current_path) + '/.pack.json'
        index_service = Index()
        index_service.run((current_path, ))
        path_list = index_service.get_list()
        self._configurator.update_config('path_list', path_list)
        self._configurator.save(config_path)
