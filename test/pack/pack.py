from pathlib import Path

from services.copy import Copy
from services.delete import Delete
from services.zip import Zip


def copy_main():
    current_path = str((Path('.')).cwd())
    copy = Copy(Path(current_path + '/test/'))
    copy.run([Path(current_path)])


def zip_main():
    current_path = str((Path('.')).cwd())
    zipper = Zip(Path(current_path + '/test/'))
    zipper.run([Path(current_path)])


def delete_main():
    current_path = str((Path('.')).cwd())
    cleaner = Delete()
    cleaner.run([Path(current_path + '/test/')])


def main():
    copy_main()
    # zip_main()
    # delete_main()
    pass


if __name__ == '__main__':
    main()
